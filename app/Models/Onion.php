<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Onion extends Model
{
    protected $table = "onions";
    //
    protected $fillable =[
		'onion',
    ];
}