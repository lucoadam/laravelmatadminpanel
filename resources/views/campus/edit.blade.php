@extends('layouts.app', ['activePage' => 'campus-management', 'titlePage' => __('Campus Management')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('campus.update', $campus) }}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('put')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Edit Campus') }}</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('campus.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                  </div>
                </div>
							<div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Name') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Name') }}" value="{{ old('name', $campus->name) }}" required="true" aria-required="true"/>
                      @if ($errors->has('name'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
							<div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('No_of_students') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('no_of_students') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('no_of_students') ? ' is-invalid' : '' }}" name="no_of_students" id="input-no_of_students" type="text" placeholder="{{ __('No_of_students') }}" value="{{ old('no_of_students', $campus->no_of_students) }}" required="true" aria-required="true"/>
                      @if ($errors->has('no_of_students'))
                        <span id="no_of_students-error" class="error text-danger" for="input-no_of_students">{{ $errors->first('no_of_students') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection